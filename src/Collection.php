<?php

namespace Erc\CodeIgniter\MC;

use Erc\CodeIgniter\App;
use Erc\CodeIgniter\Db;

class Collection {

	/**
	 * Таблица
	 * @var string
	 */
	protected static $table = null;

	/**
	 * Первичный ключ в таблице
	 * @var string
	 */
	protected static $primary_key = null;

	/**
	 * Набор полей для поиска
	 * Example: $fields = array('id', 'status')
	 *
	 * @var array
	 */
	protected static $fields = array();

	/**
	 * Набор полей для множественного поиска
	 * Example: $fields = array('ids' => 'id', 'status_list' => 'status')
	 *
	 * @var array
	 */
	protected static $fields_in = array();

	/**
	 * Набор полей для поиска в строке
	 * Example: $fields = array('search_name' => 'name', 'search_fullname' => 'fullname')
	 *
	 * @var array
	 */
	protected static $fields_like = array();

	/**
	 * Набор полей для исключения из поиска
	 * Example: $fields_not = array('name_not' => 'name', 'id_not' => 'id')
	 *
	 * @var array
	 */
	protected static $fields_not = array();

	/**
	 * Набор полей для поиска в строке
	 * Example: $fields_not_in = array('ids_not_in' => 'id', 'name_not_in' => 'name')
	 *
	 * @var array
	 */
	protected static $fields_not_in = array();

	/**
	 * Набор полей для сортировки
	 * Example: $fields_order = array('order_id' => 'asc', 'place_payment_id' => 'desc')
	 *
	 * @var array
	 */
	protected static $fields_order = array();

	/**
	 * Набор полей для поиска больше значения
	 * Example: $fields_not = array('date_greater' => 'date, 'amount_greater' => 'amount')
	 *
	 * @var array
	 */
	protected static $fields_greater = array();

	/**
	 * Набор полей для поиска больше значения или равно
	 * Example: $fields_not = array('date_greater' => 'date, 'amount_greater' => 'amount')
	 *
	 * @var array
	 */
	protected static $fields_greater_than_equal = array();

	/**
	 * Набор полей для поиска меньше значения
	 * Example: $fields_not = array('date_less' => 'date, 'amount_less' => 'amount')
	 *
	 * @var array
	 */
	protected static $fields_less = array();

	/**
	 * Набор полей для поиска меньше значения или равно
	 * Example: $fields_not = array('date_less' => 'date, 'amount_less' => 'amount')
	 *
	 * @var array
	 */
	protected static $fields_less_than_equal = array();

	/**
	 * Переменная для возвращения коллекции моделей
	 * Example: "app\model\Company\Company"
	 * @var string
	 */
	protected static $model = null;

	/**
	 * База данных
	 * @var string
	 */
	protected static $db = null;

	/**
	 * Получение списка данных
	 * @param array $params
	 * @return array
	 */
	public static function getList($params = array()) {
		Db::setDb(static::$db);

		App::db(static::$db)->select('*')
			->from(static::$table);

		Db::where(static::$fields, $params);

		Db::where_in(static::$fields_in, $params);

		Db::where_not(static::$fields_not, $params);

		Db::where_not_in(static::$fields_not_in, $params);

		Db::where_greater(static::$fields_greater, $params);
		Db::where_greater_than_equal(static::$fields_greater_than_equal, $params);

		Db::where_less(static::$fields_less, $params);
		Db::where_less_than_equal(static::$fields_less_than_equal, $params);

		Db::like(static::$fields_like, $params);

		if (!empty(static::$fields_order)) {
			foreach (static::$fields_order as $order => $by) {
				Db::order_by(static::$fields, $params, $order, $by);
			}
		} else {
			Db::order_by(static::$fields, $params, static::$primary_key, 'asc');
		}

		Db::limit($params);

		$result = App::db(static::$db)->get()->result_array();

		return $result;
	}

	/**
	 * Получение списка данных
	 * @param array $params
	 * @return array
	 */
	public static function getFullList($params = []) {
		return static::getList($params);
	}

	/**
	 * Получение данных
	 * @param array $params
	 * @return array
	 */
	public static function get($params = []) {
		$params['limit'] = 1;

		$result = static::getList($params);

		return ($result) ? $result[0] : [];
	}

	/**
	 * Получение полных данных
	 * @param array $params
	 * @return array
	 */
	public static function getFull($params = []) {
		$params['limit'] = 1;

		$result = static::getFullList($params);

		return ($result) ? $result[0] : [];
	}

	/**
	 * Получение массива моделей
	 * @param array $params
	 * @return array
	 */
	public static function getListModel($params = []) {
		$result = static::getList($params);

		$result_model = [];
		foreach ($result as $m) {
			$result_model = new static::$model($m);
		}

		return $result_model;
	}

	/**
	 * Получение массива моделей
	 * @param array $params
	 * @return array
	 */
	public static function getFullListModel($params = []) {
		$result = static::getFullList($params);

		$result_model = [];
		foreach ($result as $m) {
			$result_model = new static::$model($m);
		}

		return $result_model;
	}

	public static function getModel($params = []) {
		$result = static::get($params);

		return new static::$model($result);
	}

	public static function getFullModel($params = []) {
		$result = static::getFull($params);

		return new static::$model($result);
	}

	/**
	 * Получение кол-ва данных данных
	 * @param array $params
	 * @return int
	 */
	public static function getTotal($params = array()) {
		Db::setDb(static::$db);

		App::db(static::$db)->select('COUNT(*) AS count')
			->from(static::$table);

		Db::where(static::$fields, $params);

		Db::where_in(static::$fields_in, $params);

		Db::where_not(static::$fields_not, $params);

		Db::where_not_in(static::$fields_not_in, $params);

		Db::where_greater(static::$fields_greater, $params);
		Db::where_greater_than_equal(static::$fields_greater_than_equal, $params);

		Db::where_less(static::$fields_less, $params);
		Db::where_less_than_equal(static::$fields_less_than_equal, $params);

		Db::like(static::$fields_like, $params);

		$result = App::db(static::$db)->get()->row_array();

		return (int) $result['count'];
	}

	/**
	 * Удаление многих строк
	 * @param array
	 */
	public static function deleteList($ids) {
		if (empty($ids) || !is_array($ids)) {
			return;
		}

		App::db(static::$db)->where_in(static::$primary_key, array_data_to_type($ids));
		App::db(static::$db)->delete(static::$table);
	}

	/**
	 * Сохранение данных
	 * @param object $model Объект данных
	 * @parm array $data_list Набор данных для сохранения
	 */
	public function saveList(app\Model $model, $data_list) {
		$model->clearData();

		foreach ($data_list as $data) {
			try {
				$model->setData($data);
				$model->save();
			} catch (\CoreException $e) {
				throw $e;
			}
		}
	}

	/**
	 * Получение полей
	 * return array
	 */
	public static function getFields() {
		return static::$fields;
	}

	/**
	 * Получение ключа
	 * return string
	 */
	public static function getPrimaryKey() {
		return static::$primary_key;
	}

	/**
	 * Получение таблицы
	 * return string
	 */
	public static function getTable() {
		return static::$table;
	}

	/**
	 * Установка таблицы
	 * @param string $table
	 */
	private static function setTable($table) {
		if (empty(static::$table)) {
			static::$table = (string) $table;
		}
	}

	/**
	 * Установка полей
	 * @param array $fields
	 */
	private static function setFields($fields) {
		if (empty(static::$fields)) {
			static::$fields = (array) $fields;
		}
	}

	/**
	 * Установка модели
	 * @param string $model
	 */
	private static function setModel($model) {
		if (empty(static::$model)) {
			static::$model = (string) $model;
		}
	}

	/**
	 * Создание коллекции
	 * @param string $table
	 * @param array $fields
	 *
	 */
	public static function createCollection($table, $fields = array(), $model = null) {
		$collection = new Collection();

		$collection::setTable($table);

		if ($fields) {
			$collection::setFields($fields);
		}
		if ($models) {
			$collection::setModel($model);
		}

		return $collection;
	}

}
