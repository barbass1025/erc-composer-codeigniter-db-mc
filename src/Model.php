<?php

namespace Erc\CodeIgniter\MC;

use Erc\CodeIgniter\App;
use Erc\CodeIgniter\Db;

class Model {

	/**
	 * Первичный ключ в таблице
	 * @var string
	 */
	protected static $primary_key = null;

	/**
	 * Набор полей
	 * @var array
	 * @link application/libraries/Lvalidation.php
	 * Example: $fields = array(
	 * 	'field' => array(
	 * 		'default' => 0,
	 * 		'type' => 'string',
	 * 		'update' => bool, // Возможность обновлять поле (для метода update) подходят для полей типа date_add, date_edit
	 * 		'auto_update' => bool // В зависимости от типа подставится значение
	 * 		'set' => bool, // Возможность устанавливать значение (для метода insert/update)
	 * 		'auto_set' => bool // В зависимости от типа подставится значение
	 *
	 * 		'validate' => array( // Массив для валидации
	 * 			'type' => 'int',
	 * 			'max_length' => '10',
	 * 			'error' => 'error_field',
	 * 			'exist_in_collection' => array( // Массив для поиска в коллекции
	 * 				'class' => '', // Путь к классу
	 * 				'params' => array(), // Параметры для поиска, сюда по field вставится значение
	 * 			)
	 * 		)
	 * 	),
	 * )
	 */
	protected static $fields = array();

	/**
	 * Таблица
	 * @var string
	 */
	protected static $table = null;

	/**
	 * Данные записи
	 * @var array
	 * */
	protected $data = array();

	/**
	 * База данных
	 * @var string
	 */
	protected static $db = null;

	/**
	 * Переменная ссылка на коллекцию
	 * Example: "app\collection\Company\CompanyList"
	 * @var string
	 */
	protected static $collection = null;

	/**
	 * Вспомогательный класс по работе с БД
	 * @var Db
	 */
	protected $db_helper = null;

	/**
	 * Конструктор
	 * @param array $data Данные для записи
	 */
	public function __construct($data = null) {
		if ($data !== null) {
			$this->setData($data);
		}

		return $this;
	}

	/**
	 * Удаление данных
	 * @param int $id
	 * @param array $params
	 * @throw \ValidateException
	 * @return bool
	 */
	public static function delete($id = null, $params = []) {
		if ($id === null) {
			if (isset($this->data[static::$primary_key])) {
				$id = $this->data[static::$primary_key];
			} else {
				return false;
			}
		}

		try {
			$error = static::validateDelete($id, $params);
			if ($error !== true) {
				throw new \ValidateException($error);
			}
		} catch (\ValidateException $e) {
			throw $e;
		}

		Db::setDb(static::$db);
		Db::deleteData(static::$table, static::$primary_key, $id);

		return true;
	}

	/**
	 * Сохранение/обновление данных
	 * @param array $data
	 * @return int Id записи
	 */
	public function save() {
		if (!empty($this->data[static::$primary_key])) {
			return static::update();
		} else {
			return static::add();
		}
	}

	/**
	 * Добавление данных
	 * @param array $data
	 * @return int Id записи
	 */
	public function add() {
		$data_query = self::collectDataForInsert(static::$fields, $this->data);

		try {
			$validate = static::validateAdd($data_query);
			if ($validate !== true) {
				throw new \ValidateException($validate);
			}

			Db::setDb(static::$db);
			return Db::insertQuery(static::$table, $data_query, static::$primary_key);
		} catch (\CoreException $e) {
			throw $e;
		}
	}

	/**
	 * Сохранение/обновление данных
	 * @param array $data
	 * @return int Id записи
	 */
	public function update() {
		$data_query = static::collectDataForUpdate(static::$fields, $this->data);

		try {
			$validate = static::validateUpdate($data_query);
			if ($validate !== true) {
				throw new \ValidateException($validate);
			}

			Db::setDb(static::$db);
			return Db::updateQuery(static::$table, $data_query, static::$primary_key);
		} catch (\CoreException $e) {
			throw $e;
		}
	}

	/**
	 * Получение строки
	 * @param mixed (string|int) $id
	 * @return array
	 */
	public static function get($id) {
		$result = App::app()->db->select('*')
			->from(static::$table)
			->where(static::$primary_key, $id)
			->get()
			->row_array();

		return $result;
	}

	/**
	 * Получение модели записи
	 * @param mixed (string|int) $id
	 * @return app\Model
	 */
	public static function getModel($id) {
		$result = static::get($id);

		return new static($result);
	}

	/**
	 * Правила валидации для вставкм/обновления
	 * @return array
	 */
	public static function getRules() {
		$rules = [];
		foreach (static::$fields as $field => $data) {
			if (isset($data['validate'])) {
				$rules[$field] = $data['validate'];
			}
		}
		return $rules;
	}

	/**
	 * Валидация данных для сохранения
	 * @param array
	 * @return mixed (bool|string)
	 */
	public static function validateAdd($form = array()) {
		App::app()->lvalidation->setRules(static::getRules());

		try {
			return App::app()->lvalidation->runValidate($form);
		} catch (\ValidateException $e) {
			throw $e;
		}
	}

	/**
	 * Валидация данных для обновления
	 * @param array
	 * @return mixed (bool|string)
	 */
	public static function validateUpdate($form = array()) {
		$rules = static::getRules();

		foreach ($form as $field => $value) {
			if (!isset($rules[$field])) {
				continue;
			}

			App::app()->lvalidation->setRules(array($field => $rules[$field]));
			try {
				// Передаем весь массив, т.к. если есть сравнение с другими полями, то они тоже нужны
				$error = App::app()->lvalidation->runValidate($form);
			} catch (\ValidateException $e) {
				throw $e;
			}
			if ($error !== true) {
				return $error;
			}
		}


		return true;
	}

	/**
	 * Валидация данных при удалении
	 * @param int $id
	 * @param arry $params
	 * @return bool
	 */
	public static function validateDelete($id = null, $params = array()) {
		return true;
	}

	/**
	 * Установка данных
	 * @param array
	 */
	public function setData($data) {
		foreach ($data as $field => $param) {
			$this->data[$field] = (!is_array($data[$field]) && !is_object($data[$field]) && !is_null($data[$field])) ? trim($data[$field]) : $data[$field];
		}
	}

	/**
	 * Получение данных
	 * @return array
	 */
	public function getData() {
		return $this->data;
	}

	/**
	 * Получение данных для вставки
	 * @return array
	 */
	public function getDataToInsert() {
		return self::collectDataForInsert(static::$fields, $this->getData());
	}

	/**
	 * Получение данных для обновления
	 * @return array
	 */
	public function getDataToUpdate() {
		return self::collectDataForUpdate(static::$fields, $this->getData());
	}

	/**
	 * Очищение данных записи
	 */
	public function clearData() {
		$this->data = array();
	}

	/**
	 * Обработка данных для INSERT
	 * @param array $field_list Поля базы
	 * @param array $data Данные
	 * @return array
	 */
	public static function collectDataForInsert($field_list, $data) {
		$result = [];

		foreach ($field_list as $field => $param) {
			$val = null;

			if (isset($data[$field])) {
				$val = (is_string($data[$field])) ? trim($data[$field]) : $data[$field];
			} elseif (isset($param['default'])) {
				$val = $param['default'];
			} else {
				$val = null;
			}

			// Если автоподстановка разрешена, то на основе типа ставим значение
			if (!empty($param['auto_set'])) {
				if (isset($param['type']) &&
					$param['type'] === 'date' &&
					empty($data[$field])
				) {
					$val = date('Y-m-d H:i:s');
				}
			}

			// Если установка из вне запрещена
			if (isset($param['set']) &&
				$param['set'] === false
			) {
				// Смотрим дефолтный
				if (isset($param['default'])) {
					$val = $param['default'];
				}
				// Смотрим автоподстановку
				if (!empty($param['auto_set'])) {
					if (isset($param['type']) && $param['type'] === 'date') {
						$val = date('Y-m-d H:i:s');
					}
				}
			}

			if (array_key_exists('default', $param) &&
				$param['default'] === null &&
				in_array($val, ['', 'null'])
			) {
				$val = null;
			}

			$result[$field] = $val;
		}

		return $result;
	}

	/**
	 * Обработка данных для UPDATE
	 * @param array $field_list Поля базы
	 * @param array $data Данные
	 * @return array
	 */
	public static function collectDataForUpdate($field_list, $data) {
		$result = array();

		foreach ($field_list as $field => $param) {
			if (!array_key_exists($field, $data) && empty($param['auto_update'])) {
				continue;
			}

			$val = (isset($data[$field])) ? $data[$field] : null;

			// Если автоподстановка разрешена, то на основе типа ставим значение
			if (!empty($param['auto_update'])) {
				if (isset($param['type']) && $param['type'] === 'date') {
					$val = date('Y-m-d H:i:s');
				}
			}

			// Если установка из вне запрещена
			if (isset($param['update']) &&
				$param['update'] === false
			) {
				// Смотрим автоподстановку
				if (!empty($param['auto_update'])) {
					if (isset($param['type']) && $param['type'] === 'date') {
						$val = date('Y-m-d H:i:s');
					}
				} else {
					if (isset($param['default'])) {
						$val = $param['default'];
					} else {
						continue;
					}
				}
			}

			if (array_key_exists('default', $param) &&
				$param['default'] === null &&
				in_array($val, ['', 'null'])
			) {
				$val = null;
			}

			$result[$field] = (is_string($val)) ? trim($val) : $val;
		}

		return $result;
	}

	/**
	 * Установка таблицы
	 * @param string $table
	 */
	private static function setTable($table) {
		if (empty(static::$table)) {
			static::$table = (string) $table;
		}
	}

	/**
	 * Установка полей
	 * @param array $fields
	 */
	private static function setFields($fields) {
		if (empty(static::$fields)) {
			static::$fields = (array) $fields;
		}
	}

	/**
	 * Установка первичного ключа
	 * @param string $primary_key
	 */
	private static function setPrimaryKey($primary_key) {
		if (empty(static::$primary_key)) {
			static::$primary_key = (string) $primary_key;
		}
	}

	/**
	 * Установка коллекции
	 * @param string $collection
	 */
	private static function setCollection($collection) {
		if (empty(static::$collection)) {
			static::$collection = (string) $collection;
		}
	}

	/**
	 * Создание коллекции
	 * @param string $table
	 * @param array $fields
	 * @param array $primary_key
	 * @param string $collection
	 *
	 */
	public static function createModel($table, $fields = array(), $primary_key = null, $collection = null) {
		$model = new Model();

		$model::setTable($table);
		$model::setFields($fields);
		$model::setPrimaryKey($primary_key);
		$model::setCollection($collection);

		return $model;
	}

}
